package com.lianxi.ouhe.classofcoupling.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @Classname ${小柯}
 * @Description
 * @Date 2019/12/26 21:09
 * @Created by Administrator
 */
public class Jieo {
    /**
     * 程序的耦合
     * DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver()); 当mysql8.0驱动不存在的时候，编译就报错了
     * ----> 程序运行需要耦合，程序间的依赖关系
     * 包含： 类之间的依赖，方法之间的依赖。
     *
     * 解耦：降低程序间的依赖关系
     *  实际开发：
     *    应该做到，编译时依赖，运行时才依赖。
     *      解耦思路：
     *          第一步：使用反射来创建对象，而避免使用new关键字。
     *          第二步：通过读取配置文件来获取要创建对象的全限定类名
     *
     *
     */

    public static void main(String[] args) throws Exception{
        //注册驱动
        //DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        Class.forName("com.mysql.cj.jdbc.Driver");

        //获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql://120.25.236.19:3306/practice?useSSL=false&serverTimezone=Hongkong&allowMultiQueries=true","root","123456");

        //获取操作数据库的预处理对象
        PreparedStatement preparedStatement = connection.prepareStatement("select * from tb_area");

        //执行SQL  得到结果集
        ResultSet resultSet = preparedStatement.executeQuery();

        //便利结果集
        while (resultSet.next()){
            System.out.println(resultSet.getString("area_name"));
        }

        //释放资源
        resultSet.close();
        connection.close();
    }
}

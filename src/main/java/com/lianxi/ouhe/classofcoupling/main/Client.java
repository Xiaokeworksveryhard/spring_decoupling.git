package com.lianxi.ouhe.classofcoupling.main;

import com.lianxi.ouhe.classofcoupling.dao.AccountDao;
import com.lianxi.ouhe.classofcoupling.dao.impl.AccountDaoImpl;
import com.lianxi.ouhe.classofcoupling.factory.BeanFactory;
import com.lianxi.ouhe.classofcoupling.service.AccountService;
import com.lianxi.ouhe.classofcoupling.service.impl.AccountServiceImpl;

/**
 * @Classname ${小柯}
 * @Description TODO
 * @Date 2019/12/27 17:33
 * @Created by Administrator
 * 耦合只能降低，不能消除
 */
public class Client {
    public static void main(String[] args) {
        //new很强的耦合性，需要解决掉
      //  AccountService as = new AccountServiceImpl();

        for (int i = 0; i < 5; i++) {
            AccountService accountDao = (AccountService)BeanFactory.getBean("accountService");
            System.out.println(accountDao);
            accountDao.saveAccount();

        }



    }
}

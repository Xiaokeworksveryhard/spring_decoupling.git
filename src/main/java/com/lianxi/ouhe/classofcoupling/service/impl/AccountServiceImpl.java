package com.lianxi.ouhe.classofcoupling.service.impl;

import com.lianxi.ouhe.classofcoupling.dao.AccountDao;
import com.lianxi.ouhe.classofcoupling.dao.impl.AccountDaoImpl;
import com.lianxi.ouhe.classofcoupling.service.AccountService;

/**
 * @Classname ${小柯}
 * @Description TODO
 * @Date 2019/12/27 17:30
 * @Created by Administrator
 */
public class AccountServiceImpl implements AccountService {

    ////new很强的耦合性，需要解决掉
    AccountDao accountDao = new AccountDaoImpl();
    public void saveAccount(){
        accountDao.saveAccount();
    }
}
